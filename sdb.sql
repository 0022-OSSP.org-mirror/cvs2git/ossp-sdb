--
--  OSSP sdb -- Skill Database
--  Copyright (c) 2003 The OSSP Project <http://www.ossp.org/>
--  Copyright (c) 2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
--  Copyright (c) 2003 Ralf S. Engelschall <rse@engelschall.com>
--
--  This file is part of OSSP sdb, a small skill database Web UI
--  which can be found at http://www.ossp.org/pkg/tool/sdb/
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version
--  2.0 of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--  General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program; if not, write to the Free Software
--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
--  USA, or contact The OSSP Project <ossp@ossp.org>.
--
--  sdb.sql: skill database schema (language: SQLite SQL)
--

DROP TABLE sdb_person;
DROP TABLE sdb_team;
DROP TABLE sdb_skill;
DROP TABLE sdb_member;
DROP TABLE sdb_provide;

CREATE TABLE sdb_person (
    pe_id         INTEGER PRIMARY KEY,
    pe_name       TEXT,
    pe_email      TEXT,
    pe_phone      TEXT
);

CREATE TABLE sdb_team (
    te_id         INTEGER PRIMARY KEY,
    te_name       TEXT
);

CREATE TABLE sdb_skill (
    sk_id         INTEGER PRIMARY KEY,
    sk_name       TEXT
);

CREATE TABLE sdb_member (
    ms_pe_id      INTEGER,
    ms_te_id      INTEGER
);

CREATE TABLE sdb_provide (
    as_pe_id      INTEGER,
    as_sk_id      INTEGER,
    as_degree     INTEGER
);

INSERT INTO sdb_person VALUES (1,  'Peter Kajinski',        'peter@de.cw.com',         '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (2,  'Ralf S. Engelschall',   'rse@de.cw.com',           '+49-89-92699-251');
INSERT INTO sdb_person VALUES (3,  'Thomas Lotterer',       'thl@.dev.de.cw.com',      '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (4,  'Michael Schloh',        'ms@.dev.de.cw.com',       '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (5,  'Michael van Elst',      'mlelstv@.dev.de.cw.com',  '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (6,  'Christoph Schug',       'cschug@de.cw.com',        '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (7,  'Christian Muschiol',    'cmuschio@de.cw.com',      '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (8,  'Alexander W�gner',      'awaegner@de.cw.com',      '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (9,  'Manuel Hendel',         'mhendel@de.cw.com',       '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (10, 'Jost Blachnitzky',      'scholli@de.cw.com',       '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (11, 'Thomas Rohde',          'rohde@de.cw.com',         '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (12, 'Christian Botta',       'cbotta@de.cw.com',        '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (13, 'Stephan Gans',          'sgans@de.cw.com',         '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (14, 'Sebastian Gierth',      'sgierth@de.cw.com',       '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (15, 'Andrea Sikeler',        'asikeler@de.cw.com',      '+49-89-92699-xxx');
INSERT INTO sdb_person VALUES (16, 'Christian Scheithauer', 'cscheith@de.cw.com',      '+49-89-92699-xxx');

INSERT INTO sdb_team VALUES (1,  'IS');
INSERT INTO sdb_team VALUES (2,  'IS/Dev');
INSERT INTO sdb_team VALUES (3,  'IS/Hst');
INSERT INTO sdb_team VALUES (4,  'IS/DB');
INSERT INTO sdb_team VALUES (5,  'IS/Man');

INSERT INTO sdb_member VALUES (1,   1);
INSERT INTO sdb_member VALUES (2,   1);
INSERT INTO sdb_member VALUES (3,   1);
INSERT INTO sdb_member VALUES (4,   1);
INSERT INTO sdb_member VALUES (5,   1);
INSERT INTO sdb_member VALUES (6,   1);
INSERT INTO sdb_member VALUES (7,   1);
INSERT INTO sdb_member VALUES (8,   1);
INSERT INTO sdb_member VALUES (9,   1);
INSERT INTO sdb_member VALUES (10,  1);
INSERT INTO sdb_member VALUES (11,  1);
INSERT INTO sdb_member VALUES (12,  1);
INSERT INTO sdb_member VALUES (13,  1);
INSERT INTO sdb_member VALUES (14,  1);
INSERT INTO sdb_member VALUES (15,  1);
INSERT INTO sdb_member VALUES (16,  1);

INSERT INTO sdb_member VALUES (2,   2);
INSERT INTO sdb_member VALUES (3,   2);
INSERT INTO sdb_member VALUES (4,   2);
INSERT INTO sdb_member VALUES (5,   2);
             
INSERT INTO sdb_member VALUES (6,   3);
INSERT INTO sdb_member VALUES (7,   3);
INSERT INTO sdb_member VALUES (8,   3);
INSERT INTO sdb_member VALUES (9,   3);
INSERT INTO sdb_member VALUES (10,  3);
INSERT INTO sdb_member VALUES (11,  3);
              
INSERT INTO sdb_member VALUES (12,  4);
INSERT INTO sdb_member VALUES (13,  4);
INSERT INTO sdb_member VALUES (14,  4);
INSERT INTO sdb_member VALUES (15,  4);
INSERT INTO sdb_member VALUES (16,  4);
               
INSERT INTO sdb_member VALUES (1,   5);
INSERT INTO sdb_member VALUES (2,   5);
INSERT INTO sdb_member VALUES (6,   5);
INSERT INTO sdb_member VALUES (12,  5);
                
INSERT INTO sdb_skill VALUES (1,   'General :: Management');
INSERT INTO sdb_skill VALUES (2,   'General :: Education');
INSERT INTO sdb_skill VALUES (3,   'General :: Computer Science');
INSERT INTO sdb_skill VALUES (4,   'ISP :: Project Management');
INSERT INTO sdb_skill VALUES (5,   'ISP :: Datacenter Infrastructure');
INSERT INTO sdb_skill VALUES (6,   'ISP :: Backbone');
INSERT INTO sdb_skill VALUES (7,   'Languages :: Command');
INSERT INTO sdb_skill VALUES (8,   'Languages :: Programming');
INSERT INTO sdb_skill VALUES (9,   'Languages :: Markup');
INSERT INTO sdb_skill VALUES (10,  'Applications :: Frontend');
INSERT INTO sdb_skill VALUES (11,  'Applications :: Middleware');
INSERT INTO sdb_skill VALUES (12,  'Applications :: Backend');
INSERT INTO sdb_skill VALUES (13,  'Operating Systems :: Unix');
INSERT INTO sdb_skill VALUES (14,  'Operating Systems :: Windows');
INSERT INTO sdb_skill VALUES (15,  'Networking :: OSI 5-7');
INSERT INTO sdb_skill VALUES (16,  'Networking :: OSI 3-4');
INSERT INTO sdb_skill VALUES (17,  'Networking :: OSI 1-2');
INSERT INTO sdb_skill VALUES (18,  'Hardware :: Server');
INSERT INTO sdb_skill VALUES (19,  'Hardware :: Storage');
INSERT INTO sdb_skill VALUES (20,  'Hardware :: Network');

INSERT INTO sdb_provide VALUES (1,  1, 2);
INSERT INTO sdb_provide VALUES (2,  1, 3);
INSERT INTO sdb_provide VALUES (6,  1, 3);
INSERT INTO sdb_provide VALUES (12, 1, 4);

